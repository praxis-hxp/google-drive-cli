package api

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/drive/v2"
)

func Login(credentialsURL, accessTokenPath string) *http.Client {
	resp, err := http.Get(credentialsURL)

	if err != nil {
		log.Fatalf("Error while getting CLI credentials: %v \n", err)
	}

	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Error while getting CLI credentials \n")
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Error while getting CLI credentials: %v \n", err)
	}

	config, err := google.ConfigFromJSON(body, "https://www.googleapis.com/auth/drive")
	if err != nil {
		log.Fatalf("Error while getting CLI credentials: %v \n", err)
	}

	return GetClient(config, accessTokenPath)
}

func Logout() {
	os.Remove(TokenFileHomeDir())
}

func Export(srv *drive.Service, fileID, mimeType, outputFile string) error {
	res, err := srv.Files.Export(fileID, mimeType).Download()
	if err != nil {
		log.Fatalf("Unable to export file: %v", err)
		return err
	}

	file := os.Stdout
	if outputFile != "" {
		file, err = os.Create(outputFile)
	}

	defer file.Close()

	if err != nil {
		log.Fatalf("Unable to export file: %v", err)
		return err
	}

	_, err = io.Copy(file, res.Body)
	return err
}

func List(srv *drive.Service, directoryID string) error {
	q := srv.Files.List()
	if directoryID != "" {
		q.Q("'" + directoryID + "' in parents")
	}

	files, err := q.Do()
	if err != nil {
		log.Fatalf("Unable to list files: %v", err)
		return err
	}

	for _, file := range files.Items {
		fmt.Printf("%s;%s\n", file.Id, file.OriginalFilename)
	}

	return err
}
