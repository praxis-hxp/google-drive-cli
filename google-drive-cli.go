/*
Copyright © 2022 Carlos Ochoa <cochoa@praxis.cl>

*/
package main

import "gitlab.com/praxis-hxp/google-drive-cli/cmd"

func main() {
	cmd.Execute()
}
