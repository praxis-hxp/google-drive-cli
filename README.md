# Google Drive CLI

Google Drive CLI is a simple CLI based on [Cobra](https://github.com/spf13/cobra) for perform Google Drive API operations like export, list, and so on. Requires Golang v1.16 or newer.

# Overview

```
google-drive-cli login
google-drive-cli export fileID --mime-type text/plain --output-file document.txt
google-drive-cli logout
```

# Installing
```
go install gitlab.com/praxis-hxp/google-drive-cli@latest
```

# Login workflow

* Google Drive CLI follow the new Google Oauth method
* After `google-drive-cli login` you will see a link for authenticate with your google account and give access to this project to your files
* upon access granting, you need to copy the token from the URL and paste it in the console.

Example:
given a returning Google url:
```
http://localhost/?state=state-token&code=4/0AWswdrc44l9F1k8MDoxSQ-SwEr5PLo1htV8kQJCep74ZXdpb_k58jjIbU5TqlSB7TDNTUA&scope=https://www.googleapis.com/auth/drive
```

the code you need to copy is:
```
4/0AWswdrc44l9F1k8MDoxSQ-SwEr5PLo1htV8kQJCep74ZXdpb_k58jjIbU5TqlSB7TDNTUA
```

## Use cases

It's useful for performing automation tasks in shell scripts

For example: given a countries Google Document with fileID = 1SqDW6uwE43KCn4-cVvffTLfl2x0uRSSHbETEw5Qe1wI
```
 google-drive-cli export 1SqDW6uwE43KCn4-cVvffTLfl2x0uRSSHbETEw5Qe1wI --mime-type text/plain | bash process_countries.bash
```

## Usage

```
google-drive-cli help
```

#### Feel free to contribute


# License
It's released under the GPL V3 license. See [LICENSE.txt](https://gitlab.com/praxis-hxp/google-drive-cli/-/blob/main/LICENSE.txt)