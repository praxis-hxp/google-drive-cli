package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/praxis-hxp/google-drive-cli/api"
)

func init() {
	rootCmd.AddCommand(logoutCmd)
}

var logoutCmd = &cobra.Command{
	Use:   "logout",
	Short: "Logout removes access token from $HOME/.google-drive-cli.json",
	Run: func(cmd *cobra.Command, args []string) {
		logout()
	},
}

func logout() {
	api.Logout()
}
