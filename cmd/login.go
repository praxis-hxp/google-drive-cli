package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/praxis-hxp/google-drive-cli/api"
)

func init() {
	rootCmd.AddCommand(loginCmd)
}

var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Login with Google OAuth and retreive the access token",
	Long: `You need the access token to perform any API operations on your Google Drive.
	The login action trigger a browser OAuth flow to Authorize this app and get the token.
	The access token is stored in $HOME/.google-drive-cli.json
	`,
	Run: func(cmd *cobra.Command, args []string) {
		login()
	},
}

func login() {
	api.Login(credentialsURL, accessTokenPath)
}
