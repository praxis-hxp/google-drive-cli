package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/praxis-hxp/google-drive-cli/api"
)

var (
	listCmd = &cobra.Command{
		Use:   "list fileID",
		Short: "Lists a fileID from Google Drive API",
		Long:  ` You can give a folder ID for listing the files in this folder`,
		Run: func(cmd *cobra.Command, args []string) {
			list(args)
		},
	}
)

func init() {
	rootCmd.AddCommand(listCmd)

	listCmd.Flags().StringVarP(&directoryID, "directory", "d", "", "directory to list. Google Drive root if not provided")
}

func list(args []string) {
	client := api.Login(credentialsURL, accessTokenPath)
	ctx = context.Background()
	service := api.GetService(ctx, client)
	api.List(service, directoryID)
}
