package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/praxis-hxp/google-drive-cli/api"
)

var (
	exportCmd = &cobra.Command{
		Use:   "export fileID",
		Short: "Exports a fileID from Google Drive API",
		Long: `
		`,
		Args: cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			export(args)
		},
	}
)

func init() {
	rootCmd.AddCommand(exportCmd)

	exportCmd.Flags().StringVarP(&mimeType, "mime-type", "t", "", "format of the exported file. See https://developers.google.com/drive/api/guides/ref-export-formats for all available exporting mime types")
	exportCmd.Flags().StringVarP(&outputFile, "output-file", "o", "", "path-to-exported-file. os.stdOUT if no provided")
}

func export(args []string) {
	client := api.Login(credentialsURL, accessTokenPath)
	ctx = context.Background()
	service := api.GetService(ctx, client)
	fileID = args[0]
	api.Export(service, fileID, mimeType, outputFile)
}
