/*
Copyright © 2022 Carlos Ochoa <cochoa@praxis.cl>

*/
package cmd

import (
	"context"
	"os"

	"github.com/spf13/cobra"
)

var (
	ctx             context.Context
	credentialsURL  = "https://gitlab.com/praxis-hxp/google-drive-cli/-/raw/main/auth/credentials.json?inline=false"
	accessTokenPath string

	fileID      string
	mimeType    string
	outputFile  string
	directoryID string

	rootCmd = &cobra.Command{
		Use:   "google-drive-cli",
		Short: "A brief description of your application",
		Long:  `This CLI is intended to simply access to Google Drive API to make regular file operations`,
	}
)

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&accessTokenPath, "access-token", "", "google access token json (default is $HOME/.google-drive-cli.json)")
}
